Er is één gebruiker aangemaakt in de database:
userid: admin
password: 1234

Een vergelijkbare competitie systeem is: http://mbvvolley.nl. De regels daar kun je als uitgangspunt gebruiken.

Known issues:
1. Er zijn dingen die een niet ingelogde gebruiker kan zien die die niet hoort te zien, bijv  de link 'uitslag' bij bepaalde wedstrijden
1. Per speelweek horen alle rondes en velden zichtbaar te zijn, ook als er geen wedstrijden ingepland zijn dan.
1. Uitslagen pagina klopt niet: er staan uitslagen in de database, maar zijn niet zichtbaar
1. Verschillende gegevens, zoals leden en wedstrijden, kunnen niet worden ingevoerd door de admin
1. Idee: kunnen wedstrijden niet worden 'gegenereerd' door een algoritme, aan het begin van het speelseizoen?
1. Gegevens kunnen ook niet worden bewerkt
1. Wens: elk lid krijgt ook een account. Als hij inlogt: iets als een "mijn team"  
