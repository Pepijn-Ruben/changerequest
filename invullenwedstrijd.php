<!-- VOEG NOG DE SPEELWEEK TOE ALS VARIABELE VOOR IN DE DATABASE EN ZORG DAN DAT DIE UITGEVOERD KAN WORDEN -->
<?php
// Sessies en Autorisatie
require_once 'tools/security.php';
if (!isAuthenticated()) {
	header("HTTP/1.1 403 Unauthorized");
	header("Location: 403.php");
	exit;
}

$speelweek = $_GET['speelweek'];

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli =  get_mysqli();

$sqlteam = "SELECT * FROM team";
$teams = $mysqli->query($sqlteam);
$sqlteam2 = "SELECT * FROM team";
$teams2 = $mysqli->query($sqlteam);
$sqlteam3 = "SELECT * FROM team";
$teams3 = $mysqli->query($sqlteam);

	if(isset($_POST['ronde'])){
		if($_POST['ronde'] != null){
			$ronde = $_POST['ronde'];
		}
			if(isset($_POST['veld'])){
				if($_POST['veld'] != null){
					$veld = $_POST['veld'];
				}
			
					if(isset($_POST['team_a'])){
						if($_POST['team_a'] != null){
							$team_a = $_POST['team_a'];
						}
					
							if(isset($_POST['team_b'])){
								if($_POST['team_b'] != null){
									$team_b = $_POST['team_b'];
								}
							
									if(isset($_POST['scheids'])){
										if($_POST['scheids'] != null){
											$scheids = $_POST['scheids'];
										}
									
									// $speelweek = 1;
									$insertSQL = "INSERT INTO wedstrijd (speelweek, ronde, veld, team_a, team_b, scheids)
													VALUES ($speelweek, $ronde, $veld, $team_a, $team_b, $scheids) ";
									$result = $mysqli->query($insertSQL);
									}
							}
					}
			}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<div class="well"><h1>Invullen Wedstrijd</h1></div>
				<?php
					if (isset($_POST['submit'])) {
					echo '<p class="psucc">Succes! Datum is toegevoegd aan de lijst</p>';
					}
				?>

			<form method="POST">
				<div class="panel panel-default">
					<div class="panel-body">
						<table class="table table-condensed table-striped">
							<tr>
								<th class="col-sm-1">Ronde</th>
								<th class="col-sm-1">Veld</th>
								<th class="col-sm-3">Team A</th>
								<th class="col-sm-3">Team B</th>
								<th class="col-sm-2">Scheidsrechter/teller</th>

							</tr>
							<tr>
								<td>
									<select name="ronde" class="form-control form-control-sm">
										<option value="" disabled selected hidden>ronde</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</td>
								<td>
									<select name="veld" class="form-control form-control-sm">
										<option value=""disabled selected hidden>veld</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
									</select>
								</td>
								<?php
								echo('<td><select name="team_a" class="form-control form-control-sm"><option value="" disabled selected hidden>Kies een optie</option>');
									while ( $rowteam = $teams->fetch_assoc() ) {
										echo('<option value="'.$rowteam["id"].'">'.$rowteam["naam"].'</option>');
										}
										echo('</select></td>');

										echo('<td><select name="team_b" class="form-control form-control-sm"><option value="" disabled selected hidden>Kies een optie</option>');
									while ( $rowteam2 = $teams2->fetch_assoc() ) {
										echo('<option value="'.$rowteam2["id"].'">'.$rowteam2["naam"].'</option>');
										}
										echo('</select></td>');

										echo('<td><select name="scheids" class="form-control form-control-sm"><option value="" disabled selected hidden>Kies een optie</option>');
									while ( $rowteam3 = $teams3->fetch_assoc() ) {
										echo('<option value="'.$rowteam3["id"].'">'.$rowteam3["naam"].'</option>');
										}
										echo('</select></td>');
								?>
								</td>
						</table>
						</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary">Opslaan</button>
					</div>
				</div>
			</form>
		</main>
	</body>
</html> 