<?php
    // Sessies en Autorisatie
    require_once 'tools/security.php';

    // Nodig als de database wordt gebruikt in dit script
	require_once 'tools/db.php';
	$mysqli =  get_mysqli();

	// Speeltijden ophalen
    $tijden = Array();
    $sql = "SELECT * FROM Ronde";
    $resTijden = $mysqli->query($sql);
    while($rowTijd = $resTijden->fetch_assoc()) {
        $tijden[$rowTijd['id']] = $rowTijd['tijd'];
    }

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<div class="well"><h1>Wedstrijdschema</h1><?php if (isAuthenticated()) {echo "<a href='./invullenwedstrijdweek.php'>Voeg wedstrijdweek toe</a>";}?></div>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
				<?php 
					$sql = "SELECT * FROM SPEELWEEK";
					$resWeken = $mysqli->query($sql);
					if($resWeken->num_rows == 0) {
						echo '<div class="alert alert-warning" role="alert">'.
									'<i class="fa fa-exclamation-triangle"></i> Er zijn geen speelweken gevonden</div>';
					} else {
						$expanded = " in";
						while ($rowWeek = $resWeken->fetch_assoc()) { 
							$date = date("d F Y", strtotime($rowWeek['datum']));
							$panelID = 'heading'.$rowWeek['id'];
							$collapseID = 'collapse'.$rowWeek['id'];
							?>
							
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="<?php echo $panelID ?>">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $collapseID ?>" 
											aria-expanded="false" aria-controls="<?php echo $collapseID ?>">
											Speelweek <?php echo $rowWeek['id'].": ".$date ?>
										</a>
									</h4>
								</div>
								<div id="<?php echo $collapseID ?>" class="panel-collapse collapse<?php echo $expanded ?>" role="tabpanel" 
									aria-labelledby="<?php echo $panelID ?>">
									<div class="panel-body">
										<?php
										

											$speelweek = $rowWeek['id'];
											
											//Sql Team a naam en alle andere wedstrijdgegevens
											$sql = "SELECT wedstrijd.*, team.naam, team.klasse FROM wedstrijd
                                                        LEFT JOIN team ON wedstrijd.team_a = team.id WHERE speelweek = $speelweek";
											$resWedstr = $mysqli->query($sql);
											
											//Sql team b naam
											$sqlTB = "SELECT wedstrijd.*, team.naam, team.klasse FROM wedstrijd
														LEFT JOIN team ON wedstrijd.team_b = team.id WHERE speelweek = $speelweek";
											$resWedstrTB = $mysqli->query($sqlTB);

											//Sql Team van scheidsrechter
											$sqlSch = "SELECT wedstrijd.*, team.naam, team.klasse FROM wedstrijd
														LEFT JOIN team ON wedstrijd.scheids = team.id WHERE speelweek = $speelweek";
											$resWedstrSch = $mysqli->query($sqlSch);

											if(!$resWedstr || $resWedstr->num_rows == 0) {
												echo '<div class="alert alert-info" role="alert">'.
															'<i class="fa fa-info-circle"></i> Er zijn geen wedstrijden gevonden <a href="invullenwedstrijd.php?speelweek='. $rowWeek["id"] . '"><img src="images/addnew.png" class="newwedschema"></a></div>';
											} else {
										?>
										<table class="table table-condensed table-striped">
											<tr>
												<th class="col-sm-1">Tijd</th>
												<th class="col-sm-1">Veld</th>
												<th class="col-sm-1">Klas</th>
												<th class="col-sm-3">Team A</th>
												<th class="col-sm-3">Team B</th>
												<th class="col-sm-2">Scheidsrechter/teller</th>
												<th class="col-sm-1">Klas</th>
												<th class="col-sm-1">wedstrijd</th>
												<th class="col-sm-1">uitslag</th>
											</tr>
											<?php
												while(true) {
													$rowWedstr = $resWedstr->fetch_assoc();
													$rowWedstrTB = $resWedstrTB->fetch_assoc();
													$rowWedstrSch = $resWedstrSch->fetch_assoc();

													$sql = "SELECT * FROM ronde AS r";
													$result = $mysqli->query($sql);
													while($row= $result->fetch_assoc()){

													$tijden[$row["id"]]=$row["tijd"];

													}

													for ($t = 1; $t <= 3; $t++){
													for ($i = 1; $i <= 6; $i++) {
														$sql = "SELECT * FROM wedstrijd WHERE ronde = $t AND veld = $i AND speelweek = " . $rowWeek['id'];
														$result = $mysqli->query($sql);
														if ($result->num_rows == 0) {
															echo "<tr>";
															echo "<td>".$tijden[$t]."</td>";
															echo "<td>".$i."</td>";
															echo "<td>-</td>";
															echo "<td>-</td>";
															echo "<td>-</td>";
															echo "<td>-</td>";	
															echo "<td>-</td>";
															$wedstrijd_id = $rowWedstr['id'];															
															$sql = "SELECT COUNT(*) AS wedstrijd FROM wedstrijd WHERE ronde = $t AND veld=$i AND speelweek=". $rowWeek['id'];
															$res = $mysqli->query($sql);
															$row = $res->fetch_assoc();
															echo "<td>";
															if ($row['wedstrijd']==0){
																echo '<a href="invullenwedstrijd.php?speelweek='. $rowWeek["id"] . '"><img src="images/addnew.png" class="newwedschema"></a>';
															} else {
																echo '<a href="aanpassenwedstrijd.php?wedstrijdid='.$wedstrijd_id.'&week='.$rowWeek["id"].'"><img src="images/edit.png" class="editwedschema"></a>';
															}
															echo "<td>-</td>";															
															echo "</td>";
															echo "</tr>";

														} else {
															$row = $result->fetch_assoc();
															echo "<tr>";
															echo "<td>".$tijden[$t]."</td>";
															echo "<td>".$i."</td>";
															echo "<td>".$rowWedstr['klasse']."</td>";
															echo "<td>".$rowWedstr['naam']."</td>";
															echo "<td>".$rowWedstrTB['naam']."</td>";
															echo "<td>".$rowWedstrSch['naam']."</td>";	
															echo "<td>".$rowWedstrTB['klasse']."</td>";
															echo "<td>";
															$wedstrijd_id = $rowWedstr['id'];
															$sql = "SELECT COUNT(*) AS wedstrijd FROM wedstrijd WHERE ronde = $t AND veld=$i AND speelweek=". $rowWeek['id'];
															$res = $mysqli->query($sql);
															$row = $res->fetch_assoc();
															if ($row['wedstrijd']==0){
																echo '<a href="invullenwedstrijd.php?speelweek='. $rowWeek["id"] . '"><img src="images/addnew.png" class="newwedschema"></a>';
															} else {
																echo '<a href="aanpassenwedstrijd.php?wedstrijdid='.$wedstrijd_id. '&week='.$rowWeek["id"].'"><img src="images/edit.png" class="editwedschema"></a>';
															}

															echo "</td>";
															echo "<td>";

															$sql = "SELECT COUNT(*) AS uitslag FROM Uitslag_set WHERE wedstrijd=$wedstrijd_id";
															$res = $mysqli->query($sql);
															$row = $res->fetch_assoc();
															if ($row['uitslag']==0 && $rowWedstr['team_a'] != null) {
																echo '<a href="invullenuitslag.php?wedstrijdid='.$wedstrijd_id.'"><img src="images/addnew.png" class="newwedschema"></a>';
															} else {
																echo '<a href="aanpassenuitslag.php?wedstrijdid='.$wedstrijd_id.'"><img src="images/edit.png" class="editwedschema"></a>';
															
															}
															echo "</td>";
															echo "</tr>";
															$rowWedstr = $resWedstr->fetch_assoc();
															$rowWedstrTB = $resWedstrTB->fetch_assoc();
															$rowWedstrSch = $resWedstrSch->fetch_assoc();
														}
													}
												}
													break;
												}
											?>
										</table>
										<?php } // end if ?> 
									</div>
								</div>
							</div>

							<?php 
							$expanded = "";
						}
					}
				?>

		</main>
	</body>
</html>
 