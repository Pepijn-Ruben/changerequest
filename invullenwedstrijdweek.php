<?php
// Sessies en Autorisatie
require_once 'tools/security.php';
if (!isAuthenticated()) {
	header("HTTP/1.1 403 Unauthorized");
	header("Location: 403.php");
	exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
if(isset($_POST['submit'])){
    
    //check if is empty
    if(!empty($_POST['datum'])){
       
        $datum = $_POST['datum'];
        $sql2 = "SELECT MAX(`id`) as mID FROM speelweek";
        $result = $mysqli->query($sql2);
        $row = $result->fetch_assoc();
        $id = $row['mID']+1;

        $sql = "INSERT INTO speelweek (id, datum) VALUES ($id, '$datum')";
        if ($mysqli->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $mysqli->error;
        }
    }
    $mysqli->close();
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<div class="well"><h1>Invullen Wedstrijd week</h1></div>   
				<?php
					if (isset($_POST['submit'])) {
					echo '<p class="psucc">Succes! Datum is toegevoegd aan de lijst</p>';
					}
				?>
			<form method="POST">
				<div class="panel panel-default">
					<div class="panel-heading"> <h3>Voeg de datum van de nieuwe wedstrijd week in.</h3>
							<br/>
					</div>
					<div class="panel-body">
						<input type="hidden" name="wedstrijdid" value="<?php echo $wedstrijddata['id'] ?>" />
						<table class="table table-striped">
							<thead>
								<tr>
									<th rowspan="2">Datum:</th>
								</tr>
							</thead>
							<tbody>
								<tr>
                                    <th><input type="date" class="form-control" name="datum" placeholder="Datum: dd/mm/jj"></th>								</tr>
							</tbody>
						</table>
					</div>
					<div class="panel-footer">
						<button type="submit" name="submit" class="btn btn-primary">Opslaan</button>
					</div>
				</div>
			</form>
		</main>
	</body>
</html>