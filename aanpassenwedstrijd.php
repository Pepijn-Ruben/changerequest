<?php
// Sessies en Autorisatie
require_once 'tools/security.php';
if (!isAuthenticated()) {
	header("HTTP/1.1 403 Unauthorized");
	header("Location: 403.php");
	exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli =  get_mysqli();
	if(isset($_GET['wedstrijdid'])){
		$wedstrijdid =	$_GET['wedstrijdid'];
	}else{
		$wedstrijdid ='1';
	}
	if(isset($_GET['wedstrijdid'])){
		$speelweek =	$_GET['week'];
	}else{
		$speelweek ='1';
	}

		$sql = "SELECT *, t.naam AS teamnaam, w.ronde AS tijdid, t.id AS teamid FROM wedstrijd AS w, team AS t, ronde AS r WHERE t.id = w.team_a AND r.id = w.ronde AND w.id='$wedstrijdid'";
		$scores = $mysqli->query($sql);
		$sql2 = "SELECT *, t.naam, t.id AS teamid, t.naam AS teamnaam FROM wedstrijd AS w, team AS t WHERE t.id = w.team_b AND w.id='$wedstrijdid'";
		$scores2 = $mysqli->query($sql2);
		$sql3 = "SELECT *, t.naam, t.id AS teamid, t.naam AS teamnaam FROM wedstrijd AS w, team AS t WHERE t.id = w.scheids AND w.id='$wedstrijdid'";
		$scores3 = $mysqli->query($sql3);
		$sqltijd = "SELECT * FROM ronde";
		$tijden = $mysqli->query($sqltijd);
		$sqlteam = "SELECT * FROM team";
		$teams = $mysqli->query($sqlteam);
		$sqlteam2 = "SELECT * FROM team";
		$teams2 = $mysqli->query($sqlteam2);
		$sqlteam3 = "SELECT * FROM team";
		$teams3 = $mysqli->query($sqlteam3);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<div class="well"><h1>Wedstrijdschema aanpassen</h1></div>
			<form method="POST">
				<div class="panel panel-default">
					<div class="panel-body">
						<input type="hidden" name="wedstrijdid" value="<?php echo $wedstrijddata['id'] ?>" />
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Tijd</th>
									<th>Veld</th>
									<th>Klas</th>
									<th>Team_A</th>
									<th>Team_B</th>
									<th>Teller</th>
									<th>Klas</th>
								</tr>
							</thead>
							
							<tbody>
								<?php
									while ( $row = $scores->fetch_assoc() ) {
										while($row2 = $scores2->fetch_assoc()){
											while($row3 = $scores3->fetch_assoc()){	
											echo '<tr>';
											echo('<th><select name="tijd" class="form-control form-control-sm"><option value="'.$row["tijdid"].'">'.$row["tijd"].'</option>');
											while ( $rowtijd = $tijden->fetch_assoc() ) {
											echo('<option value="'.$rowtijd["id"].'">'.$rowtijd["tijd"].'</option>');
											}
											echo('</select></th>');
											echo '<th><input type="text" class="form-control" name="veld" placeholder="score" value = "' .$row['veld'] . '"></th>';
											echo '<th><input type="text" class="form-control" name="klasse1" placeholder="score" value = "' .$row['klasse'] . '"></th>';
											echo('<th><select name="team_a" class="form-control form-control-sm"><option value="'.$row["teamid"].'">'.$row["teamnaam"].'</option>');
											while ( $rowteam = $teams->fetch_assoc() ) {
											echo('<option value="'.$rowteam["id"].'">'.$rowteam["naam"].'</option>');
											}
											echo('</select></th>');

											echo('<th><select name="team_b" class="form-control form-control-sm"><option value="'.$row2["teamid"].'">'.$row2["teamnaam"].'</option>');
											while ( $rowteam2 = $teams2->fetch_assoc() ) {
											echo('<option value="'.$rowteam2["id"].'">'.$rowteam2["naam"].'</option>');
											}
											echo('</select></th>');

											echo('<th><select name="scheids" class="form-control form-control-sm"><option value="'.$row3["teamid"].'">'.$row3["teamnaam"].'</option>');
											while ( $rowteam3 = $teams3->fetch_assoc() ) {
											echo('<option value="'.$rowteam3["id"].'">'.$rowteam3["naam"].'</option>');
											}
											echo('</select></th>');
											echo '<th><input type="text" class="form-control" name="klasse2" placeholder="score" value = "' .$row2['klasse'] . '"></th>';
											echo '</tr>';
											}
										}
									}

								?>
							</tbody>
						</table>
					</div>
					<div class="panel-footer">
						<button type="submit" class="btn btn-primary">Opslaan</button> 
					</div>
				</div>
			</form>
		</main>
		

		
		<?php
		if(isset($_POST['veld'])){
		$veld = $_POST['veld'];
	}else{
		return false;
	}
	if(isset($_POST['team_a'])){
		$team_a = $_POST['team_a'];
	}else{
		return false;
	}
	if(isset($_POST['team_b'])){
		$team_b = $_POST['team_b'];
	}else{
		return false;
	}
	if(isset($_POST['scheids'])){
		$scheids = $_POST['scheids'];
	}else{
		return false;
	}
	if(isset($_POST['tijd'])){
		$tijd = $_POST['tijd'];
	}else{
		return false;
	}
	$insertSQL = "UPDATE `wedstrijd` SET  `speelweek` = '$speelweek', `ronde` = '$tijd', `veld` = '$veld', `team_a` = '$team_a', `team_b` = '$team_b', `scheids` = '$scheids' WHERE `wedstrijd`.`id` = $wedstrijdid";
	$result = $mysqli->query($insertSQL);
	?>
	</body>
</html>