<?php

require_once 'tools/db.php';
$mysqli =  get_mysqli();

$sql = "SELECT * FROM lid";
$res = $mysqli->query($sql);

$sql2 = "SELECT *, t.naam AS teamnaam FROM team_has_lid AS thl, team AS t WHERE t.id = thl.team";
$res2 = $mysqli->query($sql2);
while($row2 = $res2->fetch_assoc()) {
		$thl[$row2['lid']] = $row2['teamnaam'];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>MBV Volley</title>
	<?php include 'head.html' ?>
	<link rel="stylesheet" href="./css/style.css">
</head>
<body>
	<?php include 'header.php' ?>
	<main class="container">
		<div class="well">
			<div class="leden-header">
				<h1 class="banner">Leden</h1>
				<a href="./leden-new.php">
					<img src="./images/addnew.png" alt="edit" class="new">
				</a>
			</div>
		</div>
		<div class="panel panel-default">
			<table class='table table-striped flexedit'>
				<tr>
					<th>Namen</th>
					<th>Team</th>
				</tr>
				<?php
					while($row = $res->fetch_assoc())
					{
						echo "<tr>";
						echo "<td><a href='./leden-edit.php?id=" . $row['id'] . "'><img src='./images/edit.png' alt='edit' class='edit'></a>" . $row['naam'] . "</td>";
						if(isset($thl[$row['id']])){
							echo('<td>'.$thl[$row['id']].'</td>');
						}else {
							echo('<td>-</td>');
						}
						echo "</tr>";
					}

				echo "</table>";
				?>
			</div>
		</main>
</body>
</html>
