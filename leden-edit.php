<?php
// Sessies en Autorisatie
require_once 'tools/security.php';
if (!isAuthenticated()) {
	header("HTTP/1.1 403 Unauthorized");
	header("Location: 403.php");
	exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli = get_mysqli();

$id = $_GET['id'];

$sqlteam = "SELECT * FROM team";
$teams = $mysqli->query($sqlteam);

$sql = "SELECT naam FROM lid WHERE id = $id";
$result = $mysqli->query($sql);

if (!isset($_POST['submit'])) {
  // $sql = "SELECT naam FROM lid WHERE id = $id";
  // $result = $mysqli->query($sql);
} else {

  $name = $_POST['name'];
  $sqlnew = "UPDATE lid SET naam = '$name' WHERE id = $id";
  $mysqli->query($sqlnew);

  if (isset($_POST['team_a'])) {

    $teamadded = $_POST['team_a'];
    $teamaddsql = "UPDATE team_has_lid SET team='$teamadded' WHERE lid ='$id'";
    $mysqli->query($teamaddsql);
  }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>MBV Volley</title>
  <?php include 'head.html' ?>
  <link rel="stylesheet" href="./css/style.css">
</head>
<body>
  <?php include 'header.php' ?>
  <main class="container">
    <div class="well">
      <div class="leden-header">
      </div>
      <h1 class="banner">Leden Wijzigen</h1>
      <?php
      if (isset($_POST['name'])) {
        echo '<p class="psucc">Succes! naam is gewijzigd in de lijst</p>';
      }?>
      <?php
      echo '<form action="leden-edit.php?id=' . $id . '" method="post">';
      if (!isset($_POST['submit'])) {
        while($row = mysqli_fetch_array($result)) {
          echo 'Naam: <input type="text" name="name" value="' . $row["naam"] . '" class="form-control"><br>';
        }
      }
      else {
        echo 'Naam: <input type="text" name="name" value="" placeholder="......"><br>';
      }
      echo('<td>Team: <select name="team_a" class="form-control form-control-sm"><option value="" disabled selected hidden>Kies een optie</option>');
      while ( $rowteam = $teams->fetch_assoc() ) {
        echo('<option value="'.$rowteam["id"].'">'.$rowteam["naam"].'</option>');
      }
      echo('</select></td>');
      ?>
      <input type="submit" value="Submit" name="submit" class="btn btn-primary">
    </form>
  </div>
</main>


</body>
</html>
