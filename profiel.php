<html lang="en">
	<head>
		<title>MBV Volley</title>
        <?php include 'head.html'; 
        ?>
        <link href="./css/style.css" rel="stylesheet">
    </head>
    <body>
    <?php include 'header.php' ?>
</body>
</html>
<?php

    // Sessies en Autorisatie
    require_once 'tools/security.php';
    
        // Nodig als de database wordt gebruikt in dit script
        require_once 'tools/db.php';
        $mysqli =  get_mysqli();

        $sql = "SELECT *, t.naam AS teamnaam, l.naam AS naam FROM team_has_lid AS thl, lid AS l, team AS t WHERE l.id = thl.lid AND t.id = thl.team AND thl.team IN(SELECT team FROM team_has_lid WHERE lid = '1')";
        $resteam = $mysqli->query($sql);
        while($rowteam = $resteam->fetch_assoc()) {
            $teamname = $rowteam['teamnaam'];
            $naam[] = $rowteam['naam'];
        }
        $sql = "SELECT COUNT(*) FROM team_has_lid AS thl, lid AS l, team AS t WHERE l.id = thl.lid AND t.id = thl.team AND thl.team IN(SELECT team FROM team_has_lid WHERE lid = '1')";
        $resteam = $mysqli->query($sql);
        while($rowteam = $resteam->fetch_assoc()) {
            $teammates = $rowteam['COUNT(*)'];
        }

        echo('<main class="container">');
            echo('<div class="panel panel-default down">');         
                echo("<table class='table table-condensed table-striped table-profiel'>");
                echo("<thead><tr><td><center>".$teamname."</center></td></tr></thead><tbody>");
                for($i=0;$i<$teammates; $i++){
                    echo("<tr><td>".$naam[$i]."</td></tr>");
                }
                echo("<tr><td>totaal: ".$teammates."</td></tr></tbody>");
                echo("</table>");
            echo("</div>");
        echo("</main>");
?>
