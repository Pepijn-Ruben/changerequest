<?php
// Sessies en Autorisatie
require_once 'tools/security.php';
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<div class="well"><h1>Over de sport club</h1></div>
			<div>
			<h1>Middelburgse Bedrijfsvolleybal Vereniging</h1>
			<h4><br />Wat doet de MBV? </h4><p>De MBV organiseert een volleybalcompetitie voor teams van:Bedrijven, Buurt- / Sportverenigingen, vrije teams, enz, enz. 
			<br /><br /> <h4>Waar en wanneer? </h4>De wedstrijden worden op dinsdagavond in de sporthal "De Kruitmolen" te Middelburg gespeeld. </p>
			<br />
			<h4>Spelregels</h4>
			<p>Er wordt gespeeld volgens de NEVOBO-spelregelsEr zijn echter uitzonderingen!</p>
			<p>1. Er wordt op tijd gespeeld.</p>
			<p>2. Er worden maximaal 4 sets gespeeld.</p>
			<br />
			<h4>Inlichtingen</h4>
			<p>Wilt u informatie over de MBV-competitie dan kunt u kontakt opnemen</p>
			<h4>Met de secretsresse:</h4><p> 0118-626280 of <h4>de wedstrijdleider:</h4><p> 0118-615289</p>
			</div>

			<br/>
			<br/>				
		</main>
	</body>
</html>
