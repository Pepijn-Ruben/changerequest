<?php
// Sessies en Autorisatie
require_once 'tools/security.php';
if (!isAuthenticated()) {
	header("HTTP/1.1 403 Unauthorized");
	header("Location: 403.php");
	exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli =  get_mysqli();

$sqlteam = "SELECT * FROM team";
$teams = $mysqli->query($sqlteam);

if (isset($_POST['name'])) {
  $name = $_POST['name'];
  $sql = "INSERT INTO `lid` (`id`, `naam`) VALUES (NULL, '$name');";
  mysqli_query($mysqli,$sql); // ' ` "
  if (isset($_POST['team_a'])) {
    $lidkwierie = "SELECT id FROM lid ORDER BY id DESC";
    $idlistkwierie = $mysqli->query($lidkwierie);
    while ($rowid = $idlistkwierie->fetch_assoc()) {
      $laatsteid[] = $rowid['id'];
    }

    $teamadded = $_POST['team_a'];
    $teamaddsql = "INSERT INTO team_has_lid (`team`, `lid`) VALUES ('$teamadded', '$laatsteid[0]')";
    $mysqli->query($teamaddsql);
  }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>MBV Volley</title>
  <?php include 'head.html' ?>
  <link rel="stylesheet" href="./css/style.css">
</head>
<body>
  <?php include 'header.php' ?>
  <main class="container">
    <div class="well">
      <div class="leden-header">
      </div>
      <h1 class="banner">Leden Invoeren</h1>
      <?php
      if (isset($_POST['name'])) {
        echo '<p class="psucc">Succes! naam is toegevoegd aan de lijst</p>';
      }?>
      <form action="leden-new.php" method="post">
        Naam: <input type="text" name="name" class="form-control"><br>
        <?php
        echo('<td><select name="team_a" class="form-control form-control-sm"><option value="" disabled selected hidden>Kies een optie</option>');
        while ( $rowteam = $teams->fetch_assoc() ) {
          echo('<option value="'.$rowteam["id"].'">'.$rowteam["naam"].'</option>');
        }
        echo('</select></td>');
        ?>
        <input type="submit" value="Submit" class="btn btn-primary">

      </form>

    </div>
  </main>


</body>
</html>
