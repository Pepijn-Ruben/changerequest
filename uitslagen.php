<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli =  get_mysqli();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">
			<div class="well"><h1>Uitslagen</h1></div>

			<?php
			//function voor uitslag gegevens op te halen
			function wedstrijdQuery($id) {
				$mysqli = get_mysqli();
				return mysqli_query($mysqli,
				"SELECT s.datum, r.tijd, t.klasse, concat(u.punten_a, ' - ', u.punten_b) AS uitslag, concat(u.score_a, ' - ', u.punten_b) AS score,
				(SELECT naam FROM team WHERE id = w.team_a) AS 'teamA',
				(SELECT naam FROM team WHERE id = w.team_b) AS 'teamB'
				FROM wedstrijd w
				JOIN speelweek s ON w.speelweek = s.id
				JOIN ronde r ON w.ronde = r.id
				JOIN team t ON w.team_a && w.team_b = t.id
				JOIN uitslag_set u ON u.wedstrijd = w.id
				WHERE u.wedstrijd = $id
				ORDER BY s.datum ASC");
			}

			//function voor datum ophalen
			function grabDate($id) {
				$mysqli = get_mysqli();
				return mysqli_query($mysqli,
				"SELECT s.datum
				FROM wedstrijd w
				JOIN speelweek s ON w.speelweek = s.id
				JOIN ronde r ON w.ronde = r.id
				JOIN team t ON w.team_a && w.team_b = t.id
				JOIN uitslag_set u ON u.wedstrijd = w.id
				WHERE u.wedstrijd = $id
				ORDER BY s.datum ASC");
			}
			$maxwedstrijdsql = mysqli_query($mysqli,"SELECT MAX(wedstrijd) AS wsnummer
			FROM uitslag_set");	
			$maxwedstrijd = mysqli_fetch_assoc($maxwedstrijdsql);
			$maxWs = $maxwedstrijd['wsnummer']; 
			 ?>

			 <div class="panel panel-default">
			 <?php 	for ($i=1; $i <= $maxWs; $i++) { ?>
				 <?php
				 $result = wedstrijdQuery($i);
				 $date = grabDate($i);
				 $datum = mysqli_fetch_assoc($date);
				 $maxWs = $maxwedstrijd['wsnummer'];
				 if ($datum != NULL) {
 			 	 ?>
				<div class="panel-heading" id="accordion" role="tablist" aria-multiselectable="false">

					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i?>" aria-controls="collapse1" class="">
							<?php
								$row = mysqli_fetch_array($date);
								echo $row['datum'];
							 ?>
						</a>
					</h4>
				</div>

				<div id="collapse<?php echo $i?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1" aria-expanded="true" style="">
					<div class="panel-body">
						<?php
							echo "<div class='panel panel-default'>
							<table class='table table-condensed table-striped'>

							<tr>
							<th>Tijd</th>
							<th>Klas</th>
							<th colspan='2'style='text-align: center;'>Wedstrijd</th>
							<th>Klas</th>
							<th>Score</th>
							<th>Uitslag</th>
							</tr>
							</div>";

							while($row = mysqli_fetch_assoc($result)) {
								echo "<tr>";
								echo "<td>" . $row['tijd'] . "</td>";
								echo "<td>" . $row['klasse'] . "</td>";
								echo "<td style='text-align: center;'>" . $row['teamA'] . "</td>";
								echo "<td >" . $row['teamB'] . "</td>";
								echo "<td>" . $row['klasse'] . "</td>";
								echo "<td>" . $row['score'] . "</td>";
								echo "<td>" . $row['uitslag'] . "</td>";
								echo "</tr>";
							}
							echo "</table>";
						?>
					</div>
				</div>

			</div>
			<?php
		}
	}
			?>

		</main>
	</body>
</html>
