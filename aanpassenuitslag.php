<?php
// Sessies en Autorisatie
require_once 'tools/security.php';
if (!isAuthenticated()) {
	header("HTTP/1.1 403 Unauthorized");
	header("Location: 403.php");
	exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli =  get_mysqli();


	if ($_SERVER['REQUEST_METHOD']=='POST') {
		// even gemakkelijk, zonder validatie of andere checks
		// PAS OP!!! Gevaarlijk voor SQL-injectie of andere aanvallen en foutgevoelig
		$wedstrijdid = $_POST['wedstrijdid'];

		// Wie is team A?
		$sql = "SELECT * FROM WEDSTRIJD WHERE id=".$wedstrijdid;
		$result = $mysqli->query($sql);
		if($result->num_rows == 0) {
			die("Wedstrijd wedstrijdid is niet bekend");
		}
		$wedstrijddata = $result->fetch_assoc();		

		for ($set=1; $set<=4; $set++){
			if (strlen($_POST['S'.$set.'Sa'])>0 && 
				strlen($_POST['S'.$set.'Sb'])>0 && 
				strlen($_POST['S'.$set.'Pa'])>0 && 
				strlen($_POST['S'.$set.'Pb'])>0) {
				// overnemen van de data in lokale variabelen
                $score_a = $_POST['S'.$set.'Sa'];
                $score_b = $_POST['S'.$set.'Sb'];
                $punten_a = $_POST['S'.$set.'Pa'];
                $punten_b = $_POST['S'.$set.'Pb'];
                // updaten in de databeest
				$sql  = "UPDATE uitslag_set SET score_a = '$score_a', score_b = '$score_b', punten_a = '$punten_a', punten_b = '$punten_b' WHERE wedstrijd = '$wedstrijdid' AND `set` = '$set'";
                $result = $mysqli->query($sql);
			}
		}

		header("location: ./uitslagen.php");
		exit();
	}

	$teamid = 0;
	if(isset($_GET['wedstrijdid'])) {
		$wedstrijdid = $_GET['wedstrijdid'];
		$sql = "SELECT W.*, S.datum, R.tijd  FROM Wedstrijd W 
                INNER JOIN Speelweek S ON S.id=W.speelweek 
                INNER JOIN Ronde R ON R.id=W.ronde WHERE W.id=$wedstrijdid";
		$result = $mysqli->query($sql);
		if($result->num_rows >0) {
			$wedstrijddata = $result->fetch_assoc();
			// Haal gegevens van team a er bij
            $team_a_id = $wedstrijddata['team_a'];
            $team_a = $mysqli->query("SELECT * FROM Team Where id=$team_a_id")->fetch_assoc();
            // ... en stop dit in het resultaat array
            $wedstrijddata['teama'] = $team_a['naam'];
            // Samen met de klasse (is voor alle drie de teams gelijk)
            $wedstrijddata['klasse'] = $team_a['klasse'];
            // Haal gegevens van team b er bij ...
            $team_b_id = $wedstrijddata['team_b'];
            $team_b = $mysqli->query("SELECT * FROM Team Where id=$team_b_id")->fetch_assoc();
            // ... en stop dit in het resultaat array
            $wedstrijddata['teamb'] = $team_b['naam'];
            // Haal gegevens van het scheidsrechter team er bij ...
            $team_s_id = $wedstrijddata['scheids'];
            $team_s = $mysqli->query("SELECT * FROM Team Where id=$team_s_id")->fetch_assoc();
            // ... en stop dit in het resultaat array
            $wedstrijddata['teams'] = $team_s['naam'];
		}
		// gegevens voor de waardes in de input velden
		$sql = "SELECT `set`, score_a, score_b, punten_a, punten_b FROM uitslag_set WHERE wedstrijd=".$wedstrijdid;
		$scores = $mysqli->query($sql);
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<div class="well"><h1>Aanpassen uitslag</h1></div>
			<form method="POST">
				<div class="panel panel-default">
					<div class="panel-heading">
							<div class="row">
								<div class="col-xs-4"><strong>DATUM:</strong> <?php echo $wedstrijddata['datum'] ?></div>
								<div class="col-xs-4"><strong>TIJD:</strong> <?php echo $wedstrijddata['tijd'] ?></div>
								<div class="col-xs-2"><strong>VELD:</strong> <?php echo $wedstrijddata['veld'] ?></div>
								<div class="col-xs-2"><strong>KLAS:</strong> <?php echo $wedstrijddata['klasse'] ?></div>
							</div>
							<br/>
							<div class="row">
								<div class="col-xs-12"><strong>SCHEIDSRECHTER:</strong> <?php echo $wedstrijddata['teams'] ?></div>
							</div>
							<br/>
							<div class="row">
								<div class="col-xs-6"><strong>TEAM A:</strong> <?php echo $wedstrijddata['teama'] ?></div>
								<div class="col-xs-6"><strong>TEAM B:</strong> <?php echo $wedstrijddata['teamb'] ?></div>
							</div>
					</div>
					<div class="panel-body">
						<input type="hidden" name="wedstrijdid" value="<?php echo $wedstrijddata['id'] ?>" />
						<table class="table table-striped">
							<thead>
								<tr>
									<th rowspan="2">SET</th>
									<th colspan="2">SCORE</th>
									<th colspan="2">PUNTEN</th>
								</tr>
								<tr>
									<th>TEAM A</th>
									<th>TEAM B</th>
									<th>TEAM A</th>
									<th>TEAM B</th>
								</tr>
							</thead>
							<tbody>
								<?php

									while ( $row = $scores->fetch_assoc() ) {
										echo '<tr>';
										echo '<th><strong>' . $row['set'] . '</strong></th>';
										echo '<th><input type="text" class="form-control" name="S' .$row['set']. 'Sa" placeholder="score" value = "' .$row['score_a'] . '"></th>';
										echo '<th><input type="text" class="form-control" name="S' .$row['set']. 'Sb" placeholder="score" value = "' .$row['score_b'] . '"></th>';
										echo '<th><input type="text" class="form-control" name="S' .$row['set']. 'Pa" placeholder="punten" value = "' .$row['punten_a'] . '"></th>';
										echo '<th><input type="text" class="form-control" name="S' .$row['set']. 'Pb" placeholder="punten" value = "' .$row['punten_b'] . '"></th>';
										echo '</tr>';
									}


								?>
							</tbody>
						</table>
					</div>
					<div class="panel-footer">
						<button type="submit" class="btn btn-primary">Opslaan</button>
					</div>
				</div>
			</form>
		</main>
	</body>
</html>